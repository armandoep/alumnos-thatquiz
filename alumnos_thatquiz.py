import argparse, sys, time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import pandas as pd
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

parser = argparse.ArgumentParser()
parser.add_argument('--document', help="La ruta del documento que quiere leer", required=True)
parser.add_argument('--clase', help="El nombre de la clase que quiere ingresar", required=True)
parser.add_argument('--usuario', help="El usuario para ingresar a la plataforma", required=False)
parser.add_argument('--password', help="La contrasena para ingresar a la plataforma", required=False)
args = parser.parse_args()

archivoExcel = args.document
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

completeName = pd.read_excel(archivoExcel, usecols="A,B")
clase = args.clase

driver.get("https://www.thatquiz.org/es/")
time.sleep(2)

emailInputClick = driver.find_element(By.ID, "TCHLH")
emailInput = driver.find_element(By.ID, "TCHL")
passwordInputClick = driver.find_element(By.ID, "TCHPH")
passwordInput = driver.find_element(By.ID, "TCHP")
btnSubmit = driver.find_element(By.CLASS_NAME, "hV6")


emailInputClick.click()

if(args.usuario):
    emailInput.send_keys(args.usuario)
else:
    emailInput.send_keys("edwin_perez2010@hotmail.es")


passwordInputClick.click()
if(args.usuario and args.password):
    passwordInput.send_keys(args.password)
else:
    print("No se envio usuario o password, se usaran credenciales default")
    print("Desea continuar? (y|n)")
    continuar = input();
    continuar = continuar.lower()
    if(continuar == "y"):
        passwordInput.send_keys("cy7EAMR4TJpnm3z")
    else:
        sys.exit()


btnSubmit.click()

time.sleep(2)
frame = driver.find_element(By.ID, "fLinks")
driver.switch_to.frame(frame)
newClass = driver.find_element(By.LINK_TEXT, "Clase nueva")
newClass.click()
driver.switch_to.default_content()
driver.switch_to.frame(driver.find_element(By.ID, "fContent"))
nombreClase = driver.find_element(By.ID, "NEWCLASSNAME")
nombreClase.send_keys(clase)

for index, alumnos in enumerate(completeName.values):
    try:
        nombres = driver.find_element(By.ID, 'FN' + str(index))
    except NoSuchElementException:
        driver.find_element(By.ID, 'addMore1').click()
        time.sleep(0.5)
        nombres = driver.find_element(By.ID, 'FN' + str(index))
    else:
        print("No se agregan columnas")

    nombres.send_keys(alumnos[1])

    apellidos = driver.find_element(By.ID, 'LN' + str(index))
    apellidos.send_keys(alumnos[0])

    
    print(alumnos[1] + " " + alumnos[0] + " is loading!")
    
    time.sleep(1)

driver.find_element(By.ID, 'save1').click()
time.sleep(3)
print('Done!')
driver.close()         